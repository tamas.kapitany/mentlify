import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApplyFormComponent} from './apply-form/apply-form.component';
import {MentorListComponent} from './mentor-list/mentor-list.component';
import {MatButtonModule, MatInputModule} from '@angular/material';
import { HeaderComponent } from './header/header.component';
import {AppRoutingModule} from '../core/app-routing/app-routing.module';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [ApplyFormComponent, MentorListComponent, HeaderComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    RouterModule
  ],
  exports: [
    MentorListComponent,
    ApplyFormComponent,
    MatInputModule,
    MatButtonModule,
    HeaderComponent,
  ]
})
export class SharedModule {
}
