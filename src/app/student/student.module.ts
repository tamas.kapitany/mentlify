import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentComponent } from './student.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [StudentComponent, StudentDetailsComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class StudentModule { }
