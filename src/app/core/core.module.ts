import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {StudentModule} from '../student/student.module';
import {AuthModule} from '../auth/auth.module';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    StudentModule,
    AuthModule,
    AppRoutingModule
  ],
  exports: [
    RouterModule
  ]
})
export class CoreModule { }
