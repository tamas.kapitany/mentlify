import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StudentDetailsComponent} from '../../student/student-details/student-details.component';
import {SignUpComponent} from '../../auth/sign-up/sign-up.component';
import {SignInComponent} from '../../auth/sign-in/sign-in.component';

const appRoutes: Routes = [
  {path: '', component: SignUpComponent},
  {path: 'login', component: SignInComponent},
  {path: 'profile', component: StudentDetailsComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
